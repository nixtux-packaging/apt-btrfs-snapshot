# apt-btrfs-snapshot fork

## Changes in the fork 

Changes compared to the upstream version 3.5.1:

* Systemd daily timer instead of weekly cron to clean old snapshots
* Do not run the timer when on battery (with no AC power)
* Force btrfs sync after snapshot deletions
* Introduce etc/apt-btrfs-snapshot.conf.d/ directory for config files which are loaded in alpha-numerical order
* /etc/apt-btrfs-snapshot.conf.d/10-default.conf is the default config, but other packages may put other configs into the config directory (for example, I want to override MaxAge on a group of machines and package my own config file /etc/apt-btrfs-snapshot.conf.d/20-my.conf); I will probably add such config to [system-autoupdate](https://gitlab.com/mikhailnov/system-autoupdate) 
* Default MaxAge to 15 days (2 weeks + 1 day) instead of 90 days to reduce problems with no empty space left on disk
* Drop support for building with python2
* Depend from btrfsmaintenance of my own packaging (https://gitlab.com/nixtux-packaging/btrfsmaintenance/tree/master/debian)
* Build-depend from dh-systemd for building on Ubuntu 16.04 xenial
* Change 'python3-distutils' to 'python3-distutils-extra' to support older Debian and Ubuntu releases, including Ubuntu 16.04  
note: python3-distutils-extra depends from intltool, which depends from automake
* change 'btrfs-tools' to 'btrfs-progs | btrfs-tools'
move 'btrfsmaintenance' to recommended dependencies
* add recommended dependency from [dumasnap](https://gitlab.com/mikhailnov/dumasnap)  for daily auto backups
* add debian/watch
* edit debian/copyright
* build-depend from dh-python

Changes sent to Ubuntu developers in LP#[1778256](https://bugs.launchpad.net/ubuntu/+source/apt-btrfs-snapshot/+bug/1778256).

## Installation
Use [ppa:mikhailnov/utils](https://launchpad.net/~mikhailnov/+archive/ubuntu/utils/) or deb package attached in [Tags](https://gitlab.com/mikhailnov/apt-btrfs-snapshot/tags).

